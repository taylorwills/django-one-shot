from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm


def todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todo_lists/list.html", context)


def todo_list_detail(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {"details": details}
    return render(request, "todo_lists/details.html", context)


def todo_list_create(request):

    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_form = form.save()
            new_form.save()
            return redirect("todo_list_detail", new_form.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todo_lists/create.html", context)


def todo_list_update(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "todo_list": todo_list,
        "form": form,
    }
    return render(request, "todo_lists/update.html", context)


def todo_list_delete(request, id):
    form = TodoList.objects.get(id=id)
    if request.method == "POST":
        form.delete()
        return redirect("todo_list_list")

    return render(request, "todo_lists/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            instance = form.save()
            return redirect("todo_list_detail", instance.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todo_items/create.html", context)


def todo_item_update(request, id):
    item = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoItemForm(instance=item)

    context = {
        "item": item,
        "form": form,
    }
    return render(request, "todo_items/update.html", context)
