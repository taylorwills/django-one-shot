from django import forms
from .models import TodoList, TodoItem


class TodoListForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = "__all__"


class TodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = "__all__"


# class TaskCreateForm(forms.ModelForm):
#     class Meta:
#         model = TodoItem
#         fields = [
#             "task",
#             "due_date",
#             "is_completed",
#             "list",
#         ]


# class UpdateItemForm(forms.ModelForm):
#     class Meta:
#         model = TodoItem
#         fields = [
#             "task",
#             "due_date",
#             "is_completed",
#             "list",
#         ]
